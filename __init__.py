from st3m.application import Application, ApplicationContext
from st3m.input import InputState
from st3m import logging
from st3m.goose import Optional
from st3m.power import machine
from ctx import Context

import leds
import st3m.run
import ntptime
import network
import time
import sys
import os
import math

log = logging.Log(__name__, level=logging.INFO)
log.info("time")

TIMEZONE = 2  # this is camp, here and now we are +2 hours to UTC
NTPTIMEOUT = 10  # how many seconds to wait between NTP attempts


# Format of the time tuple:
#       (year, mon, mday, hour, min, sec, wday, yday)
# e.g., (2023, 8, 18, 11, 38, 45, 4, 230)
TIME_INDEX_HOUR = 3
TIME_INDEX_MIN = 4
TIME_INDEX_SEC = 5

HAND_START_HOUR = 11
HAND_LENGTH_HOUR = 59
HAND_WIDTH_HOUR = 13
HAND_COLOR_HOUR = (1, 0, 0)

HAND_START_MIN = 16
HAND_LENGTH_MIN = 102
HAND_WIDTH_MIN = 8
HAND_COLOR_MIN = (1, 0, 0)

HAND_START_SEC = 27
HAND_LENGTH_SEC = 112
HAND_WIDTH_SEC = 2
HAND_COLOR_SEC = (1, 1, 1)


def initialize_wlan(ssid="Camp2023-open"):
    wlan = network.WLAN(network.STA_IF)
    if wlan.isconnected():
        log.info("wlan is already connected, not trying again")
        return wlan
    wlan.active(True)
    try:
        wlan.connect(ssid)
    except OSError as e:
        log.error(f"initialize_wlan failed")
        log.error(e)
        machine.soft_reset()  # "Wifi Internal Error" (non recoverable)
    while not wlan.isconnected():
        log.info("still trying to connect...")
        time.sleep(1)
    log.info("initialize_wlan: connected")
    log.info(wlan.ifconfig())
    return wlan


# TODO: add UI feedback when connecting to WLAN + NTP


class AnalogWatch(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._image_path = self.find_image_path()
        log.info(f"found image: {self._image_path}")
        self._time = (0, 0, 0, 0, 0, 0, 0, 0)
        self._time_str = "00:00:00"
        self.ntp_synced = False
        self.init_ntp()

        self._scale = 1.0
        self._led = 0.0
        self._phase = 0.0

    def find_image_path(self):
        # FIXME: app_ctx.bundle_path does not work (yet) as of firmware v1.1.0
        # Workaround: Search known paths and app names
        paths = [path for path in sys.path if path.startswith("/")]
        app_names = ["analog_watch"]

        for path in paths:
            for app_name in app_names:
                image_path = path + "/apps/" + app_name + "/watchface.png"

                # Try to load image from known paths and use first result
                try:
                    os.stat(image_path)
                    return image_path
                except OSError:
                    pass
        return None

    def on_enter(self, vm: Optional["ViewManager"]) -> None:
        self.init_ntp()

    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 80
        ctx.font = ctx.get_font_name(5)

        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        # Paint the watch face
        if self._image_path:
            ctx.image(self._image_path, -120, -120, 240, 240)

        # Paint the digital time
        # ctx.rgb(1, 1, 1)
        # ctx.move_to(0, 0)
        # ctx.save()
        # ctx.text(self._time_str)
        # ctx.restore()

        # Draw the hands
        seconds = self._time[TIME_INDEX_SEC]
        minutes = self._time[TIME_INDEX_MIN]
        hours = self._time[TIME_INDEX_HOUR]
        fraction_sec = seconds / 60.0
        fraction_min = (fraction_sec + minutes) / 60.0
        fraction_hour = (fraction_min + hours) / 12.0
        self.draw_hand(ctx, fraction_hour, HAND_START_HOUR, HAND_LENGTH_HOUR, HAND_WIDTH_HOUR, HAND_COLOR_HOUR)
        self.draw_hand(ctx, fraction_min, HAND_START_MIN, HAND_LENGTH_MIN, HAND_WIDTH_MIN, HAND_COLOR_MIN)
        self.draw_hand(ctx, fraction_sec, HAND_START_SEC, HAND_LENGTH_SEC, HAND_WIDTH_SEC, HAND_COLOR_SEC)

        # Draw small circle in the middle
        ctx.rgb(1, 1, 1).arc(0, 0, 6, 0, 2 * math.pi, False).fill()

        # glow LEDs
        leds.set_hsv(int(self._led), abs(self._scale) * 360, 1, 0.2)

        leds.update()

    def draw_hand(self, ctx: Context, rotation_fraction, hand_start, hand_length, hand_width, hand_color) -> None:
        rotation = math.pi / 2 - rotation_fraction * 2 * math.pi
        ctx.move_to(math.cos(rotation + math.pi) * hand_start, -math.sin(rotation + math.pi) * hand_start)
        ctx.line_to(math.cos(rotation) * hand_length, -math.sin(rotation) * hand_length)
        ctx.rgb(*hand_color)
        ctx.line_width = hand_width
        ctx.stroke()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        self._time = t = time.localtime(time.time() + TIMEZONE * 3600)
        # TODO no rjust in micropython?
        self._time_str = ":".join(map(lambda x: str(x) if x >= 10 else f"0{x}", t[3:6]))

        self._phase += delta_ms / 1000
        self._scale = math.sin(self._phase)
        self._led += delta_ms / 45
        if self._led >= 40:
            self._led = 0

    def init_ntp(self) -> None:
        if self.ntp_synced:
            return
        # wlan = initialize_wlan()
        initialize_wlan()
        log.info(f"starting to set up ntp...")
        while True:
            try:
                ntptime.settime()
                break
            except OSError as e:
                if e.args[0] == 116:  # ETIMEDOUT error code
                    log.info(f"ETIMEDOUT. Retrying after {NTPTIMEOUT} seconds...")
                    time.sleep(NTPTIMEOUT)
                else:
                    log.error(e)  # Log any other OSError
        log.info(f"...ntp successfully set up")
        self.ntp_synced = True
        # Don't disconnect for now (we don't know if we connected ourselves or
        # if the flow3r is set to "always connect", so let's be safe and keep
        # the connection)
        # wlan.disconnect()


if __name__ == "__main__":
    st3m.run.run_view(AnalogWatch(ApplicationContext()))
